<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\Todo;

class TodoController extends Controller
{
    public function index() {
        $todos = Todo::all();

        return view('todos', compact('todos'));
    }

    public function store(Request $request) {
        Todo::create(['value' => $request->input('todo')]);

        $todos = Todo::all();
        return redirect()->route('todo.index', ['todos' => $todos]);
    }

    public function update(Request $request, Todo $todo) {
        $todo->value = $request->input('todo');
        $todo->save();

        $todos = Todo::all();
        return redirect()->route('todo.index', ['todos' => $todos]);
    }

    public function destroy(Todo $todo) {
        $todo->delete();

        $todos = Todo::all();
        return redirect()->route('todo.index', ['todos' => $todos]);
    }

}
