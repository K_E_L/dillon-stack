<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Todos') }}
        </h2>
    </x-slot>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <form method="post" action="{{url('todo')}}">
                        @csrf
                        <input type="text" placeholder="Create a todo..." name="todo" autofocus>
                        <button>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @foreach ($todos as $todo)
        <div class="py-4">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 text-gray-900">
                        <form method="post" action="{{url('todos/' . $todo->id)}}">
                            @csrf
                            @method('patch')
                            <input
                                type="text"
                                value="{{ $todo->value }}"
                                placeholder="Enter a todo..."
                                name="todo"
                                autofocus
                            />
                            <button>Edit</button>
                        </form>
                        <form method="post" action="{{url('todos/' . $todo->id)}}">
                            @method('delete')
                            @csrf
                            <button>Delete</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</x-app-layout>
